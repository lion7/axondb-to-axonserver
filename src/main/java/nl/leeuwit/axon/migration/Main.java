package nl.leeuwit.axon.migration;

import io.axoniq.axondb.client.AxonDBConfiguration;
import org.axonframework.axonserver.connector.AxonServerConfiguration;

public class Main {
    public static void main(String[] args) throws Exception {
        AxonDBConfiguration axonDBConfiguration = AxonDBConfiguration.newBuilder("localhost").build();
        AxonServerConfiguration axonServerConfiguration = AxonServerConfiguration.builder().servers("localhost").build();
        Migration migration = new Migration(axonDBConfiguration, axonServerConfiguration);
        migration.migrate();
    }
}
