package nl.leeuwit.axon.migration;

import io.axoniq.axondb.Event;
import io.axoniq.axondb.client.AxonDBClient;
import io.axoniq.axondb.client.AxonDBConfiguration;
import io.axoniq.axondb.client.util.FlowControllingStreamObserver;
import io.axoniq.axondb.grpc.EventWithToken;
import io.axoniq.axondb.grpc.GetEventsRequest;
import io.grpc.stub.StreamObserver;
import org.axonframework.axonserver.connector.AxonServerConfiguration;
import org.axonframework.axonserver.connector.AxonServerConnectionManager;
import org.axonframework.axonserver.connector.event.AppendEventTransaction;
import org.axonframework.axonserver.connector.event.AxonServerEventStoreClient;
import org.axonframework.eventsourcing.eventstore.EventStoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

public class Migration {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final AxonDBConfiguration axonDBConfiguration;

    private final AxonDBClient axonDBClient;
    private final AxonServerEventStoreClient axonServerClient;

    public Migration(AxonDBConfiguration axonDBConfiguration, AxonServerConfiguration axonServerConfiguration) {
        this.axonDBConfiguration = axonDBConfiguration;
        this.axonDBClient = new AxonDBClient(axonDBConfiguration);
        AxonServerConnectionManager axonServerConnectionManager = new AxonServerConnectionManager(axonServerConfiguration);
        this.axonServerClient = new AxonServerEventStoreClient(axonServerConfiguration, axonServerConnectionManager);
    }

    public void migrate() throws Exception {
        long nextToken = axonServerClient.getLastToken().get().getToken() + 1;
        EventBuffer consumer = createConsumer(nextToken);
        while (consumer.hasNextAvailable(10, TimeUnit.SECONDS)) {
            io.axoniq.axonserver.grpc.event.EventWithToken eventWithToken = consumer.nextAvailable();
            publish(eventWithToken.getEvent());
        }
    }

    private void publish(io.axoniq.axonserver.grpc.event.Event event) throws Exception {
        AppendEventTransaction sender = axonServerClient.createAppendEventConnection();
        sender.append(event);
        sender.commit();
    }

    private EventBuffer createConsumer(long nextToken) {
        EventBuffer consumer = new EventBuffer(axonDBConfiguration.getHeartbeatInterval());

        logger.info("open stream: {}", nextToken);
        StreamObserver<GetEventsRequest> requestStream = axonDBClient.listEvents(new StreamObserver<>() {

            @Override
            public void onNext(EventWithToken eventWithToken) {
                if (Event.getDefaultInstance() == eventWithToken.getEvent()) {
                    // this is a hearbeat
                    logger.debug("Heartbeat received...");
                    consumer.touch();
                } else {
                    logger.debug("Received event with token: {}", eventWithToken.getToken());
                    consumer.push(eventWithToken);
                }
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error("Failed to receive events", throwable);
                consumer.fail(new EventStoreException("Error while reading events from the server", throwable));
            }

            @Override
            public void onCompleted() {
                consumer.fail(new EventStoreException("Connection to server has been closed"));
            }
        });
        FlowControllingStreamObserver<GetEventsRequest> observer = new FlowControllingStreamObserver<>(
                requestStream, axonDBConfiguration,
                t -> GetEventsRequest.newBuilder().setNumberOfPermits(t).build(),
                t -> false
        );

        GetEventsRequest request = GetEventsRequest.newBuilder()
                .setTrackingToken(nextToken)
                .setNumberOfPermits(axonDBConfiguration.getInitialNrOfPermits())
                .setHeartbeatInterval(axonDBConfiguration.getHeartbeatInterval())
                .build();
        observer.onNext(request);

        consumer.registerCloseListener(e -> observer.onCompleted());

        consumer.registerConsumeListener(observer::markConsumed);

        return consumer;
    }

}