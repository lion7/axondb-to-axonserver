import org.gradle.kotlin.dsl.extra
import org.gradle.kotlin.dsl.repositories

plugins {
    application
}

tasks {
    wrapper {
        distributionType = Wrapper.DistributionType.ALL
        gradleVersion = "5.0"
    }
}

repositories {
    jcenter()
}

dependencies {
    implementation("io.axoniq:axondb-client:1.3.6")
    implementation("org.axonframework:axon-server-connector:4.0.3")
    implementation("org.axonframework:axon-configuration:4.0.3")

    implementation("org.slf4j:slf4j-jdk14:1.7.25")
}

application {
    mainClassName = "nl.leeuwit.axon.migration.Main"
}
